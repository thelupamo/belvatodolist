import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { TodoService } from '../shared/todo.service';
import { Todo } from '../shared/todo.model';

declare var M: any;

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css'],
  providers: [TodoService]
})
export class TodoComponent implements OnInit {

  constructor(private todoservice: TodoService) { }

  ngOnInit() {
    this.resetForm();
    this.refreshTodoList();
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
      
    this.todoservice.selectedTodo = {
      _id: "",
      title: "",
      task: "",
      completed: true
    }
  }

  onSubmit(form : NgForm) {
    if(form.value._id == "") {
    this.todoservice.postTodo(form.value).subscribe((res) => {
      this.resetForm(form);
      this.refreshTodoList();
      M.toast({ html: 'Todo Created Successfully', classes: 'rounded' });
    }); 
}
else {
  this.todoservice.postTodo(form.value).subscribe((res) => {
    this.resetForm(form);
    this.refreshTodoList();
    M.toast({ html: 'Todo Updated Successfully', classes: 'rounded' });
  }); 
}
}

  refreshTodoList() {
  this.todoservice.getTodoList().subscribe((res) => {
    this.todoservice.todos = res as Todo[];
  });
}

onDelete(_id, string, form: NgForm) {
  if (confirm('Are you sure to delete this record?')== true) {
    this.todoservice.deletetodos(_id).subscribe((res) => {
       this.refreshTodoList();
       this.resetForm(form);
       M.toast({ html: 'ToDo Deleted successfully', classes: 'rounded' });
    });
  }
}
}


