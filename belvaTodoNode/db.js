const mongoose = require('mongoose');
const url = 'mongodb://localhost:27017/belvaToDo';

mongoose.connect(url, { 
    useNewUrlParser: true ,
    useUnifiedTopology: true
},
    (err) => {
    if (!err)
        console.log('MongoDB Connection to DB Succeeded');
    else
        console.log('Error in DB connection:' + JSON.stringify(err, undefined, 2));
});


module.exports = mongoose;