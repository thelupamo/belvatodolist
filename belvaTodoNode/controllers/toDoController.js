const express = require('express');
var router = express.Router();

var ObjectId = require('mongoose').Types.ObjectId;
var { TodoModel } = require('../models/toDo');

router.get('/', (req, res) => {
    TodoModel.find((err, docs) => {
        if (!err) { res.send(docs); }
        else { console.log('No Todo item created :' + JSON.stringify(err, undefined, 2)); }
    });
});


router.post('/', (req, res) => {
    var todo = new TodoModel({
       title: req.body.title,
       task: req.body.task,
       completed: req.body.completed
    });
    todo.save((err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Failure to create Todo item :' + JSON.stringify(err, undefined, 2)); }
    });
});


router.delete('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No todo list with given id : ${req.params.id}`);

    TodoModel.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in ToDo delete :' + JSON.stringify(err, undefined, 2)); }
    });
});



module.exports = router;

