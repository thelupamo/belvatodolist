const mongoose = require('mongoose');

var TodoModel = mongoose.model('Todo', {
    title: { type: String },
    task: { type: String },
    completed: { type: Boolean },
});

module.exports = { TodoModel };