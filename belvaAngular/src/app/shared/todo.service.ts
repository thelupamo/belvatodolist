import { Injectable } from '@angular/core';
import { Todo } from './todo.model';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  selectedTodo: Todo;
  todos: Todo[];
  readonly baseURL = 'http://localhost:3000/todo';

  constructor(private http : HttpClient) { }

  postTodo(todo : Todo) {
    return this.http.post(this.baseURL, todo);
  }

  getTodoList() {
    return this.http.get(this.baseURL);
  }

  deletetodos(_id: string) {
    return this.http.delete(this.baseURL + `/${_id}`);
  }
   

}

